<?php

namespace App\Controller;

use App\Entity\Panier;
use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MainController extends AbstractController
{
    /**
     * @Route("/collection", name="main")
     */
    public function index(): Response
    {

        $produits = $this->getDoctrine()->getRepository(Produit::class)->findAll();

        return $this->render('front/collection.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * @Route("/panier", name="panier")
     */
    public function panier(): Response
    {

        $panier = $this->getDoctrine()->getRepository(Panier::class)->findAll();

        return $this->render('front/panier.html.twig', [
            'panier' => $panier,
        ]);
    }

        /**
     * @Route("/detail", name="detail")
     */
    public function detail(): Response
    {

        $detail = $this->getDoctrine()->getRepository(Produit::class)->findAll();

        return $this->render('front/detail.html.twig', [
            'detail' => $detail,
        ]);
    }

}
