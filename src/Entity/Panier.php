<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Panier
 *
 * @ORM\Table(name="panier", indexes={@ORM\Index(name="commande_fk", columns={"id_commande"}), @ORM\Index(name="utilisateurs_fk", columns={"id_utilisateurs"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\PanierRepository")
 */
class Panier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_panier", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPanier;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_panier", type="date", nullable=false)
     */
    private $datePanier;

    /**
     * @var string
     *
     * @ORM\Column(name="sous_total", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $sousTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_total_avant_marge", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $montantTotalAvantMarge;

    /**
     * @var int
     *
     * @ORM\Column(name="taux_de_marge", type="integer", nullable=false)
     */
    private $tauxDeMarge;

    /**
     * @var string
     *
     * @ORM\Column(name="montan_total_effectif", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $montanTotalEffectif;

    /**
     * @var \Commande
     *
     * @ORM\ManyToOne(targetEntity="Commande")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_commande", referencedColumnName="id_commande")
     * })
     */
    private $idCommande;

    /**
     * @var \Utilisateurs
     *
     * @ORM\ManyToOne(targetEntity="Utilisateurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateurs", referencedColumnName="id_utilisateurs")
     * })
     */
    private $idUtilisateurs;

    public function getIdPanier(): ?int
    {
        return $this->idPanier;
    }

    public function getDatePanier(): ?\DateTimeInterface
    {
        return $this->datePanier;
    }

    public function setDatePanier(\DateTimeInterface $datePanier): self
    {
        $this->datePanier = $datePanier;

        return $this;
    }

    public function getSousTotal(): ?string
    {
        return $this->sousTotal;
    }

    public function setSousTotal(string $sousTotal): self
    {
        $this->sousTotal = $sousTotal;

        return $this;
    }

    public function getMontantTotalAvantMarge(): ?string
    {
        return $this->montantTotalAvantMarge;
    }

    public function setMontantTotalAvantMarge(string $montantTotalAvantMarge): self
    {
        $this->montantTotalAvantMarge = $montantTotalAvantMarge;

        return $this;
    }

    public function getTauxDeMarge(): ?int
    {
        return $this->tauxDeMarge;
    }

    public function setTauxDeMarge(int $tauxDeMarge): self
    {
        $this->tauxDeMarge = $tauxDeMarge;

        return $this;
    }

    public function getMontanTotalEffectif(): ?string
    {
        return $this->montanTotalEffectif;
    }

    public function setMontanTotalEffectif(string $montanTotalEffectif): self
    {
        $this->montanTotalEffectif = $montanTotalEffectif;

        return $this;
    }

    public function getIdCommande(): ?Commande
    {
        return $this->idCommande;
    }

    public function setIdCommande(?Commande $idCommande): self
    {
        $this->idCommande = $idCommande;

        return $this;
    }

    public function getIdUtilisateurs(): ?Utilisateurs
    {
        return $this->idUtilisateurs;
    }

    public function setIdUtilisateurs(?Utilisateurs $idUtilisateurs): self
    {
        $this->idUtilisateurs = $idUtilisateurs;

        return $this;
    }

    public function __toString()
    {
        return $this->idPanier . '-' . $this->idUtilisateurs;
    }


}
