<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\CommandeRepository")
 */
class Commande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_commande", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCommande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_commande", type="date", nullable=false)
     */
    private $dateCommande;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_total", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $montantTotal;

    /**
     * @var int
     *
     * @ORM\Column(name="taux_de_marge", type="integer", nullable=false)
     */
    private $tauxDeMarge;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_total_effectif", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $montantTotalEffectif;

    public function getIdCommande(): ?int
    {
        return $this->idCommande;
    }

    public function getDateCommande(): ?\DateTimeInterface
    {
        return $this->dateCommande;
    }

    public function setDateCommande(\DateTimeInterface $dateCommande): self
    {
        $this->dateCommande = $dateCommande;

        return $this;
    }

    public function getMontantTotal(): ?string
    {
        return $this->montantTotal;
    }

    public function setMontantTotal(string $montantTotal): self
    {
        $this->montantTotal = $montantTotal;

        return $this;
    }

    public function getTauxDeMarge(): ?int
    {
        return $this->tauxDeMarge;
    }

    public function setTauxDeMarge(int $tauxDeMarge): self
    {
        $this->tauxDeMarge = $tauxDeMarge;

        return $this;
    }

    public function getMontantTotalEffectif(): ?string
    {
        return $this->montantTotalEffectif;
    }

    public function setMontantTotalEffectif(string $montantTotalEffectif): self
    {
        $this->montantTotalEffectif = $montantTotalEffectif;

        return $this;
    }

    public function __toString()
    {
        return $this->idCommande . '-' . $this->dateCommande . '-' . $this->montantTotalEffectif;
    }


}
