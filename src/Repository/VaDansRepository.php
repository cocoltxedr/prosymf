<?php

namespace App\Repository;

use App\Entity\VaDans;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VaDans|null find($id, $lockMode = null, $lockVersion = null)
 * @method VaDans|null findOneBy(array $criteria, array $orderBy = null)
 * @method VaDans[]    findAll()
 * @method VaDans[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VaDansRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VaDans::class);
    }

    // /**
    //  * @return VaDans[] Returns an array of VaDans objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VaDans
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
